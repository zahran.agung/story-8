from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
from .models import Post

# Create your tests here.

class Accordion_UnitTest(TestCase):

    def test_url_index_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
    
    def test_views_index_func_used(self):
        found = resolve('/')
        self.assertEqual(found.func, index)
    
    def test_template_index_used(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')

    def test_model_create_new_content(self):
        Post.objects.create(
            title='About Me', 
            content='Name'
            )
        content_count = Post.objects.all().count()
        self.assertEqual(content_count, 1)

