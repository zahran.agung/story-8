asgiref==3.2.7
astroid==2.3.3
certifi==2020.4.5.1
chardet==3.0.4
click==7.1.1
coreapi==2.3.3
coreapi-cli==1.0.9
coreschema==0.0.4
coverage==5.0.4
dj-database-url==0.5.0
Django==3.0.5
django-cors-headers==3.2.1
django-filter==2.2.0
django-nose==1.4.6
djangorestframework==3.11.0
gunicorn==20.0.4
idna==2.9
isort==4.3.21
itypes==1.1.0
Jinja2==2.11.1
lazy-object-proxy==1.4.3
MarkupSafe==1.1.1
mccabe==0.6.1
nose==1.3.7
psycopg2-binary==2.8.5
pylint==2.4.4
pytz==2019.3
PyYAML==5.3.1
requests==2.23.0
selenium==3.141.0
six==1.14.0
sqlparse==0.3.1
uritemplate==3.0.1
urllib3==1.25.8
whitenoise==5.0.1
wrapt==1.11.2
